# Summary

## Prepare for pytest

```
pipenv install --dev pytest pytest-cov
```

## Run pytest with code coverage analysis

```
pytest --cov=vorlesung --cov-report=term-missing tests/
```

The sample output for this project is

```
» pytest --cov=vorlesung --cov-report=term-missing tests/
=================================================================================== test session starts ===================================================================================
platform darwin -- Python 3.9.12, pytest-7.1.2, pluggy-1.0.0
rootdir: /Users/Stephan.Mueller/tmp/zusammenfassung
plugins: cov-3.0.0
collected 8 items

tests/test_functions.py ..                                                                                                                                                          [ 25%]
tests/test_shop.py ...                                                                                                                                                              [ 62%]
tests/test_webserver.py ...                                                                                                                                                         [100%]

---------- coverage: platform darwin, python 3.9.12-final-0 ----------
Name                     Stmts   Miss  Cover   Missing
------------------------------------------------------
vorlesung/__init__.py        0      0   100%
vorlesung/functions.py       6      0   100%
vorlesung/shop.py           16      1    94%   48
vorlesung/webserver.py      21      5    76%   33-35, 41, 45
------------------------------------------------------
TOTAL                       43      6    86%
```

The output shows, that line `48` in file `vorlesung/shop.py` and lines `33-35, 41, 45` in file `vorlesung/webserver.py` are not covered. Because of the missing tests, the total test coverage is 86%.
