"""Module for testing webserver routes."""

from vorlesung.webserver import app


def test_hello_world():
    """Test hello world route."""
    with app.test_client() as test_app:
        response = test_app.get("/")
        assert response.status_code == 200
        assert response.json == {
            "msg": "Hello World!"
        }


def test_list_route():
    """Test list route."""
    with app.test_client() as test_app:
        response = test_app.get("/list")
        assert response.json == ["Hello", "World!", "random"]

        response = test_app.get("/list")
        assert response.json == ["Hello", "World!", "random", "random"]

        response = test_app.get("/list")
        assert response.json == ["Hello", "World!", "random", "random", "random"]


def test_not_found():
    """This is a test which does not increase test coverage but tests the webserver
    default behaviour when a path is not found.
    """
    with app.test_client() as test_app:
        response = test_app.get("/notfound")
        assert response.status_code == 404
