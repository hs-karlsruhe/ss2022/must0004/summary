from vorlesung.functions import summe, crazy_multiply


def test_summe():
    assert summe(1, 3) == 4


def test_crazy_multiply():
    assert crazy_multiply(2, 3) == 2
    assert crazy_multiply(2, 4) == 2
    assert crazy_multiply(3, 3) == 9
