from vorlesung.shop import app


def test_hello_world():
    """Test hello world route."""
    with app.test_client() as test_app:
        response = test_app.get("/")
        assert response.status_code == 403
        assert response.json == {
            "msg": "Hello World!"
        }


def test_shopping_cart():
    """Test shopping cart route.

    Check if status code is 406 and error message is correct when no article number is given.
    """
    with app.test_client() as test_app:
        response = test_app.get("/shoppingCart")
        assert response.status_code == 406
        assert response.json == {
            "msg": "Please give an article number."
        }

        response = test_app.get("/shoppingCart?articleNumber=37587")
        assert response.status_code == 200
        assert response.json == {
            "msg": "The article 37587 was added to the shopping cart."
        }


def test_get_shopping_cart():
    """Test get shopping cart route."""

    with app.test_client() as test_app:
        response = test_app.get("/getShoppingCart")
        response.status_code == 200
        response.json == {
            "15786": {
                "name": "Rucksack",
                "farbe": "rot",
                "preis": 27.99
            },
            "8537": {
                "name": "Geldbeutel",
                "groeße": "XL",
                "preis": 19.99
            }
        }
