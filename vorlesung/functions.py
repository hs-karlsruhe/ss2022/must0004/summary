"""This is a collection of some math functions."""


def summe(summand1, summand2):
    """Calculates the sum of two summands.

    :param summand1: First summand
    :param summand2: Second summand
    :return: Sum of both summands
    """
    return summand1 + summand2


def crazy_multiply(x, y):
    """Calculates the multiplication of two numbers with some magic.
    When parameter x is set to 2, the return value will always be 2.

    :param x: Faktor 1
    :param y: Faktor 2
    :return: Multiplikation der beiden Faktoren
    """
    if x == 2:
        return 2
    return x * y
