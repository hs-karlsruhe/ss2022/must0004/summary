"""This is a tiny webserver providing a simple list."""

from flask import Flask, request, jsonify


app = Flask(__name__)


RANDOM_LIST = ["Hello", "World!"]

ORDERS = []


@app.route('/')
def hello_world():
    """Return a simple hello world message."""
    return {
        "msg": "Hello World!"
    }


@app.route('/list')
def list_route():
    """Return a list with custom elements. But before returning the list, the
    list will be appended with a new item named 'random'."""
    RANDOM_LIST.append("random")
    return jsonify(RANDOM_LIST)


@app.route('/saveTopping')
def save_topping():
    """Save topping in a list of orders."""
    topping1 = request.args.get("topping1")
    ORDERS.append(topping1)
    return topping1


@app.route('/getOrders')
def get_orders():
    """Returns the list of orders."""
    return jsonify(ORDERS)


if __name__ == "__main__":
    app.run(debug=False, host="0.0.0.0", port=8080)
