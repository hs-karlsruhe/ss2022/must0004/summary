from flask import Flask, request

app = Flask(__name__)


@app.route('/')
def hello_world():
    """Returns a hello world message with status code 403."""
    return {"msg": "Hello World!"}, 403


@app.route('/shoppingCart')
def shopping_cart():
    """Adds an article to a shopping cart.

    Returns error message with status code 406 when no article number is given.
    """
    article_number = request.args.get("articleNumber")

    if article_number is None:
        return {
            "msg": "Please give an article number."
        }, 406

    return {
        "msg": "The article " + article_number + " was added to the shopping cart."
    }


@app.route("/getShoppingCart")
def get_shopping_cart():
    """Returns a sample shopping cart object."""
    return {
        "15786": {
            "name": "Rucksack",
            "farbe": "rot",
            "preis": 27.99
        },
        "8537": {
            "name": "Geldbeutel",
            "groeße": "XL",
            "preis": 19.99
        }
    }


if __name__ == "__main__":
    app.run(debug=False, host="0.0.0.0", port=8080)
